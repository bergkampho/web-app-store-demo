import { injectGlobal } from 'styled-components';
import reset from 'styled-reset';

const baseStyles = () => injectGlobal`
  ${reset}
  html {
    font-size: 14px;
  }

  body {
    margin: 0;
    padding: 0;
    font-family: "SF Pro Text", "SF Pro Icons", "Helvetica Neue", "Helvetica", "Arial", sans-serif;
    line-height: 120%;
  }
`;

export default baseStyles;
