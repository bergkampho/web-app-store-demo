import styled from 'styled-components';

const AppIcon = styled.img`
  border-radius: 10px;
`;

export default AppIcon;
