import styled from 'styled-components';
import { font } from '../constants/theme';

const Heading = styled.h1`
  font-size: ${font.xlarge};
  line-height: 150%;
  margin-bottom: 0.5em;
`;

export default Heading;
