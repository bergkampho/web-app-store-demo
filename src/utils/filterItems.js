import filter from 'lodash/filter';

const filterItems = (items, keyword) => filter(
  items,
  item =>
    item['im:name'].label.toLowerCase().indexOf(keyword) > -1 ||
    item.category.attributes.term.toLowerCase().indexOf(keyword) > -1 ||
    item.summary.label.indexOf(keyword) > -1 ||
    item['im:artist'].label.toLowerCase().indexOf(keyword) > -1,
);

export default filterItems;
