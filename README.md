# Web App Store

Demo React Redux with [Styled Components](https://www.styled-components.com/)

This project is bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

## Quick Start

Install dependencies and start developement

```bash
npm install && npm run start
```

## Lint

Lint the source to ensure coding style

```bash
npm run lint
```

## Test

Test the behaviour of Compoment

```bash
npm run test
```

## Build

Complie and optimise source to production build

```bash
npm run build
```

## Docker Deployment

Use Docker to contain a static Nginx web server

```bash
# Build Image
docker build -t web-app-store .

# Run Image
docker run -p 5000:80 -d web-app-store
```
