import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { filterApp } from '../actions/app';
import SearchSection from '../components/SearchSection';
import SearchInput from '../components/SearchInput';

const Search = (props) => (
  <SearchSection>
    <SearchInput onChange={props.filterApp} />
  </SearchSection>
);

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => bindActionCreators({
  filterApp,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Search);
