import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import AppIcon from './AppIcon';
import AppType from './AppType';


const Wrapper = styled.div`
  width: 75px;
`;

const Name = styled.div`
  display: -webkit-box;
  -webkit-line-clamp: 2;
  -webkit-box-orient: vertical;
  overflow: hidden;
  text-overflow: ellipsis;
`;

const AppItem = (props) => (
  <Wrapper>
    <AppIcon src={props.image} alt={props.name} />
    <Name>{props.name}</Name>
    <AppType>{props.type}</AppType>
  </Wrapper>
);

AppItem.propTypes = {
  name: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
};

export default AppItem;
