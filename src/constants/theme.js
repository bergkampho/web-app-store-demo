export const color = {
  'grey-darker': '#e4e5e6',
  'grey-dark': '#f9f9f9',
  grey: '#818182',
  'grey-light': '#dcdcdc',
  'grey-lighter': '#e9e9e9',
};

export const font = {
  xlarge: '1.4rem',
  large: '1.1rem',
  normal: '1rem',
  small: '0.8rem',
};
