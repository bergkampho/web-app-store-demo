import React, { Component } from 'react';
import { Provider } from 'react-redux';

import baseStyles from './baseStyles';
import Main from './components/Main';
import Search from './containers/Search';
import ScrollPanel from './containers/ScrollPanel';
import store from './store';


class App extends Component {
  render() {
    baseStyles();
    return (
      <Provider store={store}>
        <Main>
          <Search />
          <ScrollPanel />
        </Main>
      </Provider>
    );
  }
}

export default App;
