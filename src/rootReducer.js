import { combineReducers } from 'redux'
import store from './reducers/store'

export default combineReducers({
  store,
})