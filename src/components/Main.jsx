import styled from 'styled-components';

const Main = styled.main`
  margin: auto;
  max-width: 750px;
  height: 100vh;
  overflow: hidden;
`;

export default Main;
