import styled from 'styled-components';

import { color } from '../constants/theme';

const SearchSection = styled.section`
  background-color: ${color['grey-dark']};
  border-bottom: 1px solid ${color['grey-light']};
  text-align: center;
`;

export default SearchSection;
