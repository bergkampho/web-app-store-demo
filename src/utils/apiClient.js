import axios from 'axios';

const apiClient = axios.create({
  baseURL: 'https://itunes.apple.com/hk',
  timeout: 10000,
});

export default apiClient;
