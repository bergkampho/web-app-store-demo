import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import noop from 'lodash/noop';
import { Scrollbars } from 'react-custom-scrollbars';

import Recommendation from './Recommendation';
import List from './List';
import { getMoreTopFreeApp } from '../actions/app';

class ScrollPanel extends Component {
  static propTypes = {
    getMoreTopFreeApp: PropTypes.func,
    isOnSearch: PropTypes.bool,
  };

  static defaultProps = {
    getMoreTopFreeApp: noop,
    isOnSearch: false,
  };

  constructor(props, context) {
    super(props, context);
    this.handleScrollFrame = this.handleScrollFrame.bind(this);
  }

  handleScrollFrame(values) {
    const { top } = values;
    // Load more items when scroll to bottom and not on search
    if (top === 1 && !this.props.isOnSearch) {
      this.props.getMoreTopFreeApp();
    }
  }

  render() {
    return (
      <Scrollbars
        autoHide
        autoHeight
        autoHeightMax="90vh"
        onScrollFrame={this.handleScrollFrame}
      >
        <Recommendation />
        <List />
      </Scrollbars>
    );
  }
}

const mapStateToProps = state => ({
  isOnSearch: (state.store.keyword.length > 0),
});

const mapDispatchToProps = dispatch => bindActionCreators({
  getMoreTopFreeApp,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ScrollPanel);
