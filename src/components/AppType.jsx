import styled from 'styled-components';
import { color, font } from '../constants/theme';

const AppType = styled.div`
  font-size: ${font.small};
  color: ${color.grey};
  margin-top: 0.25rem;
`;

export default AppType;
