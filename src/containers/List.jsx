import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import styled from 'styled-components';
import noop from 'lodash/noop';

import { getTopFreeApp } from '../actions/app';
import { color } from '../constants/theme';
import ListItem from '../components/ListItem';
import NoResult from '../components/NoResult';
import filterItems from '../utils/filterItems';

const Wrapper = styled.div`
  border-top: 1px solid ${color['grey-light']};
`;

const ListItemWrapper = styled.div`
  :nth-child(even) {
    img {
      border-radius: 100%;
    }
  }
`;

class List extends Component {
  static propTypes = {
    getTopFreeApp: PropTypes.func,
    items: PropTypes.array,
  };

  static defaultProps = {
    getTopFreeApp: noop,
    items: [],
  };

  componentDidMount() {
    this.props.getTopFreeApp();
  }

  renderResult() {
    if (this.props.items.length === 0) {
      return <NoResult />;
    }

    return this.props.items.map((item, key) => (
      <ListItemWrapper key={item.id.attributes['im:id']}>
        <ListItem
          index={key + 1}
          name={item['im:name'].label}
          type={item.category.attributes.label}
          image={item['im:image'][0].label}
        />
      </ListItemWrapper>
    ));
  }

  render() {
    return (
      <Wrapper>
        {this.renderResult()}
      </Wrapper>
    );
  }
}

const mapStateToProps = state => ({
  items: filterItems(state.store.pagination.items, state.store.keyword),
});

const mapDispatchToProps = dispatch => bindActionCreators({
  getTopFreeApp,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(List);
