export default {
  store: {
    keyword: '',
    grossingApps: [],
    freeApps: [],
    pagination: {
      items: [],
      page: 0,
    },
  },
};
