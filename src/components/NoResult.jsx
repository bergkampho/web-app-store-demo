import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  padding: 4.5rem 1rem;
  text-align: center;
`;

const NoResult = () => <Wrapper>沒有結果</Wrapper>;

export default NoResult;
