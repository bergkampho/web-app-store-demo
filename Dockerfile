FROM node:8.9 as builder
WORKDIR /node/src
COPY . /node/src
RUN npm install
RUN npm run build
RUN npm prune -- production

FROM kyma/docker-nginx
EXPOSE 80
COPY --from=builder /node/src/build/ /var/www
CMD 'nginx'
