import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import noop from 'lodash/noop';
import styled from 'styled-components';
import Flex, { FlexItem } from 'styled-flex-component';
import { Scrollbars } from 'react-custom-scrollbars';

import { getTopGrossingApp } from '../actions/app';
import AppItem from '../components/AppItem';
import Heading from '../components/Heading';
import NoResult from '../components/NoResult';
import filterItems from '../utils/filterItems';


const Wrapper = styled.div`
  padding: 0.5rem 0.5rem 0;
`;

const FlexAppItem = styled(FlexItem)`
  margin-right: 0.75rem;
`;

class Recommendation extends Component {
  static propTypes = {
    getTopGrossingApp: PropTypes.func,
    items: PropTypes.array,
  };

  static defaultProps = {
    getTopGrossingApp: noop,
    items: [],
  };

  componentDidMount() {
    this.props.getTopGrossingApp();
  }

  renderResult() {
    if (this.props.items.length === 0) {
      return <NoResult />;
    }

    return (
      <Scrollbars autoHeight autoHeightMin={145}>
        <Flex>
          {
            this.props.items.map((item) => (
              <FlexAppItem key={item.id.attributes['im:id']}>
                <AppItem
                  name={item['im:name'].label}
                  type={item.category.attributes.label}
                  image={item['im:image'][1].label}
                />
              </FlexAppItem>
            ))
          }
        </Flex>
      </Scrollbars>
    );
  }

  render() {
    return (
      <Wrapper>
        <Heading>推介</Heading>
        {this.renderResult()}
      </Wrapper>
    );
  }
}

const mapStateToProps = state => ({
  items: filterItems(state.store.grossingApps, state.store.keyword),
});

const mapDispatchToProps = dispatch => bindActionCreators({
  getTopGrossingApp,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Recommendation);
