import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Flex, { FlexItem } from 'styled-flex-component';
import { color, font } from '../constants/theme';
import AppIcon from './AppIcon';
import AppType from './AppType';

const Wrapper = styled.div`
  padding: 0.5rem;
  border-bottom: 1px solid ${color['grey-lighter']};
`;

const Icon = styled(AppIcon)`
  margin: 0 1rem;
`;

const Index = styled.div`
  font-size: ${font.xlarge};
  color: ${color.grey};
`;

const Name = styled.div`
  font-size: ${font.large};
`;

const ListItem = (props) => (
  <Wrapper>
    <Flex alignCenter>
      <FlexItem>
        <Index>{props.index}</Index>
      </FlexItem>
      <FlexItem>
        <Icon src={props.image} alt={props.name}/>
      </FlexItem>
      <FlexItem contentStart>
        <Name>{props.name}</Name>
        <AppType>{props.type}</AppType>
      </FlexItem>
    </Flex>
  </Wrapper>
);

ListItem.propTypes = {
  index: PropTypes.number,
  name: PropTypes.string,
  type: PropTypes.string,
  image: PropTypes.string,
};

export default ListItem;
