import {
  FILTER_APP,
  GET_MORE_TOP_FREE_APP,
  GET_TOP_FREE_APP_SUCCESS,
  GET_TOP_GROSSING_APP_SUCCESS,
} from '../constants/actionTypes';
import initialState from './initialState';

const PAGE_SIZE = 10;

// IMPORTANT: Note that with Redux, state should NEVER be changed.
// State is considered immutable. Instead,
// create a copy of the state passed and set new values on the copy.
// Note that I'm using Object.assign to create a copy of current state
// and update values on the copy.
export default function storesReducer(state = initialState.store, action) {
  const newState = Object.assign({}, state);

  switch (action.type) {
    case GET_TOP_FREE_APP_SUCCESS: {
      const items = action.data.feed.entry;
      newState.freeApps = items;
      newState.pagination.items = items.slice(0, PAGE_SIZE);
      newState.pagination.page = 1;
      return newState;
    }
    case GET_MORE_TOP_FREE_APP: {
      const page = state.pagination.page + 1;
      newState.pagination.items = state.freeApps.slice(0, page * PAGE_SIZE);
      newState.pagination.page = page;
      return newState;
    }
    case GET_TOP_GROSSING_APP_SUCCESS:
      newState.grossingApps = action.data.feed.entry;
      return newState;
    case FILTER_APP:
      newState.keyword = action.data.toLowerCase();
      return newState;
    default:
      return state;
  }
}
