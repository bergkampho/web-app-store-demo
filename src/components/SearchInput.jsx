import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { color } from '../constants/theme';

const Input = styled.input`
  border-radius: 5px;
  background-color: ${color['grey-darker']};
  width: 95%;
  margin: 0.5rem;
  padding: 0.5em;
  box-sizing: border-box;
  border-width: 0px;
  border-style: initial;
  border-color: initial;
  border-image: initial;
  outline: none;
  text-align: center;
`;

class SearchInput extends Component {
  static propTypes = {
    onChange: PropTypes.func,
  };

  onChange = key => (e) => {
    this.props.onChange(e.target.value);
  };

  render() {
    return (
      <Input
        autoFocus
        type="text"
        placeholder="搜尋"
        onChange={this.onChange()}
      />
    );
  }
}

export default SearchInput;
