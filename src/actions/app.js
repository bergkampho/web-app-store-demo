import {
  FILTER_APP,
  GET_MORE_TOP_FREE_APP,
  GET_TOP_FREE_APP_FAILURE,
  GET_TOP_FREE_APP_START,
  GET_TOP_FREE_APP_SUCCESS,
  GET_TOP_GROSSING_APP_FAILURE,
  GET_TOP_GROSSING_APP_START,
  GET_TOP_GROSSING_APP_SUCCESS,
} from '../constants/actionTypes';
import apiClient from '../utils/apiClient';

export const getTopFreeApp = () => async (dispatch) => {
  dispatch({
    type: GET_TOP_FREE_APP_START,
  });

  try {
    const result = await apiClient.get('/rss/topfreeapplications/limit=100/json', {
    });

    return dispatch({
      type: GET_TOP_FREE_APP_SUCCESS,
      data: result.data,
    });
  } catch (error) {
    return dispatch({
      type: GET_TOP_FREE_APP_FAILURE,
      error,
    });
  }
};

export const getMoreTopFreeApp = () => (dispatch) => {
  dispatch({
    type: GET_MORE_TOP_FREE_APP,
  });
};

export const getTopGrossingApp = () => async (dispatch) => {
  dispatch({
    type: GET_TOP_GROSSING_APP_START,
  });

  try {
    const result = await apiClient.get('/rss/topgrossingapplications/limit=10/json', {
    });

    return dispatch({
      type: GET_TOP_GROSSING_APP_SUCCESS,
      data: result.data,
    });
  } catch (error) {
    return dispatch({
      type: GET_TOP_GROSSING_APP_FAILURE,
      error,
    });
  }
};

export const filterApp = payload => (dispatch) => {
  dispatch({
    type: FILTER_APP,
    data: payload,
  });
};
